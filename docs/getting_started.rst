Guía de Inicio
==============================================

Introducción
**************
En este apartado se presenta la guía para empezar a utilizar la API Gateway de Whatoko Health, 
la cual se utiliza para estructurar los datos de mediciones provenientes de la 
aplicación móvil, los cuales posteriormente se insertan en la base de datos.

Para utilizar el API Gateway de Whatoko Health únicamente hace falta realizar solicitudes HTTP a las rutas,
que se encuentran en la siguiente sección, con la información de las mediciones, proveniente de los fabricantes
y de las consultas al usuario.

.. note::

   Todas las respuestas provenientes de la API están en formato JSON, incluidos los errores.

Autenticación
**************

Para realizar las solicitudes HTTP a las rutas que hacen parte del API Gateway se requiere
que en el header de la solicitud haya un "x-token" que en validado por el API Gateway mediante un plugin de autenticación. Este token se crea automáticamente
cuando un usuario se registra o inicia sesión en la aplicación.
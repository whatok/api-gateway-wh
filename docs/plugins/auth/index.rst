==============================================
Plugin de autenticación
==============================================

El plugin de autenticación se encarga tomar las peticiones hechas por un cliente y verificar la existencia y validez del JWT antes de que estas sean enviadas a los servicios finales. Este plugin se encuentra implementado en todos los servicios y rutas que requieren de un servicio de auth. Las rutas de login y registro no se encuentran protegidas por el plugin.

Cuando un ciente realiza un inicio de sesión, envía sus credenciales mediante el body de la petición a la ruta de login del microservicio de autenticación:


.. http:post:: /api/v1/auth/login/

   Inicio de sesión:

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl https://localhost:8000/api/v1/auth/login/ \
              -d @body.json

   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

         {
            "email": "ana.viveros@aiatic.com", 
            "password": "#aNDev17//*"
         }

   .. note::

      :Contenido del body:

         * **email** (*string*) -- Correo electronico del usuario con el que se registró.
         * **password** (*string*) -- Contraseña asociada al correo electrónico de registro.

El API Gateway recibe la petición y la envía al microservicio de autenticación quién toma los datos de inicio de sesión y verifica que sean correctos. De ser así, firma y genera el JWT y envía una respuesta:
   
   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
        "ok": true,
        "message": "Inicio de sesión exitoso, bienvenida(o)",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIxNyIsImlhdCI6MTY4MTczOTA1MSwiZXhwIjoxNjgxODI1NDUxfQ.XXPkv79LdYAWtruwoUR-KIrTW3ycbNUCwmUrQcgvvQU",
        "name": "Ana Sofía",
        "email": "ana.viveros@aiatic.com",
        "cellphoneNumber": null,
        "id": 217,
        "rol": 3,
        "userNameAccount": null
      }

El API Gateway recibe la respuesta del microservicio y la entrega al cliente.

Una vez generado y recibido el token de autenticación, el usuario puede ahora realizar una petición a los microservicios a los que tiene acceso, si, por ejemplo, quiere realizar una inserción de calorías, este debe envíar mediante los headers de la petición el token que se generó y sus datos en el body:

.. http:post:: /api/v1/manufacturers/calories/

  Inserción de calorías 

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/calories/ \
              -d @body.json

   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "start_date": "2022-04-12T07:13:00.000Z",
            "end_date": "2022-04-13T07:12:00.000Z",
            "calories": 1058.76416015625
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **start_date** (*Date*) -- Fecha de inicio de una medición.
         * **end_date** (*Date*) -- Fecha final de una medición.
         * **calories** (*number*) -- Valor de calorías quemadas de una medición.

El API Gateway recibe la petición y verifica mediante el plugin de autenticación que la petición contenga el token y que este sea válido, de ser así, envía la petición al microservicio de fabricantes donde se realiza la inserción:

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Calories data was sent to manufacturer's microservice."
      }

De lo contrario, responde con un error antes de enviar la petición al microservicio:

    **Ejemplo de respuesta incorrecta**

    .. sourcecode:: json

        {
            "message": "Token no válido"
        }
   

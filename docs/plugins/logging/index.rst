==============================================
Plugin de manejo de Logs
==============================================

El plugin de manejo de Logs se encarga de tomar toda la información importante de las peticiones realizadas a la API de Whatoko Health y de las respuestas generadas por los servicios y enviarlas mediante una URL al servidor que se encargará de almacenarla en un archivo de Logs
Este envía la información de la siguiente manera:

**Ejemplo de formato deinformación de logs**

.. sourcecode:: json

    {
        "name": "manufacturer-service",
        "req" : {
            "method": "/api/v1/manufacturers/distance",
            "url": "/api/v1/manufacturers/distance",
            "headers": {
                "content-type": [ "application/json" ],
                "postman-token": [ "5e2f638f-0cb9-4073-a01c-3802e8d25098" ],
                "accept": [ "*/*" ],
                "accept-encoding": [ "gzip, deflate, br" ],
                "host": [ "localhost:8000" ],
                "content-length": [ "509" ],
                "connection": [ "keep-alive" ],
                "x-token": [ "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIxNyIsImlhdCI6MTY4MjUzODk4NywiZXhwIjoxNjgyNjI1Mzg3fQ.lakYUbslaSzK0w4Y_4XEOQh_lQoZKq9DuiPCCnojw-g" ],
                "user-agent": [ "PostmanRuntime/7.32.2" ]
            },
            "body": [
                {
                    "users_id": 1,
                    "device_id": "7331c9eb"
                },
                {
                    "end_date": "2022-04-09 19:12:00",
                    "distance": 37.52400207519531,
                    "start_date": "2022-11-09 18:09:26"
                },
                {
                    "end_date": "2022-11-10 14:11:06",
                    "distance": 19.573949813842773,
                    "start_date": "2022-11-10 14:11:05"
                },
                {
                    "end_date": "2022-11-11 11:44:55",
                    "distance": 2.6098599433898926,
                    "start_date": "2022-11-11 09:44:49"
                }
            ]
        },
        "res": {
            "status": 200,
            "headers": {
                "content-type": [ "application/json; charset=utf-8" ],
                "connection": [ "keep-alive" ],
                "access-control-allow-origin": [ "*" ],
                "date": [ "Thu, 27 Apr 2023 17:12:31 GMT" ],
                "etag": [ "W/\"45-5v5rYxWptR6LfgEBOvuj1oYmXJM\"" ],
                "content-length": [ "69" ],
                "x-powered-by": [ "Express" ]
            }
        }
    }



==============================================
Plugin de transformación de peticiones
==============================================

El plugin de transformación de peticiones se encarga de tomar las peticiones hechas por el cliente y añadirle headers necesarios de seguridad, almacenamiento en caché, gestión de conexiones, manejo de información del body, contenido y CORS. De la siguiente manera:

+-------------------------------+------------------------+
| ``cache-control``             |  ``no-cache``          |
+-------------------------------+------------------------+
| ``connection``                | ``keep-alive``         |
+-------------------------------+------------------------+
| ``content-type``              | ``application/json``   |
+-------------------------------+------------------------+
| ``accept``                    | ``*/*``                |
+-------------------------------+------------------------+
| ``scrict-transport-security`` | ``max-age=31536000``   |
+-------------------------------+------------------------+
| ``scrict-transport-security`` | ``includeSubDomains``  |
+-------------------------------+------------------------+



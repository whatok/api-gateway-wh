Fases del sueño
----------------------

.. http:post:: /api/v1/manufacturers/sleep/

   Estructura la información de las fases de sueño del usuario actual con el modelo de sueño e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/sleep/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         }
         {
            "start_date": "2022-06-15T23:32:00.000Z",
            "end_date": "2022-06-15T23:35:00.000Z",
            "sleep_stage": "Sueño ligero"
         },
         {
            "start_date": "2022-06-15T23:35:00.000Z",
            "end_date": "2022-06-15T23:42:00.000Z",
            "sleep_stage": "Sueño profundo"
         },
         {
            "start_date": "2022-06-15T23:42:00.000Z",
            "end_date": "2022-06-16T00:21:00.000Z",
            "sleep_stage": "Sueño ligero"
         },
         {
            "start_date": "2022-06-16T00:21:00.000Z",
            "end_date": "2022-06-16T00:25:00.000Z",
            "sleep_stage": "REM"
         },
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **sleep_stage** (*string*) -- Etapa del sueño de una medición.

            :Opciones posibles:
              - Sueño ligero
              - Sueño profundo
              - Sueño despierto
              - REM

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Sleep data was sent to manufacturer's microservice."
      }
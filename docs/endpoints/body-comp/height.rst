Altura
----------------------

.. http:post:: /api/v1/manufacturers/height/

   Estructura la información de la altura del usuario actual con el modelo de altura e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/height/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-04-12T07:13:00.000Z",
            "height": 141
         },
         {
            "date": "2022-05-06T14:11:06.635Z",
            "height": 170
         },
         {
            "date": "2022-05-31T09:44:55.798Z",
            "height": 175
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **height** (*number*) -- Valor de la altura en centímetros de una medición.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Height data was sent to manufacturer's microservice."
      }

.. note::

   La altura está medida en centimetros.
Temperatura Corporal
--------------------------

.. http:post:: /api/v1/manufacturers/body-temperature/

   Estructura la información de la temperatura corporal del usuario actual con el modelo de temperatura corporal e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/body-temperature/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-06-11T09:14:00.000Z",
            "body_temperature": 37.22222137451172
         },
         {
            "date": "2022-06-12T09:14:00.000Z",
            "body_temperature": 36.88888931274414
         },
         {
            "date": "2022-06-13T09:14:00.000Z",
            "body_temperature": 36.77777862548828
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **body_temperature** (*number*) -- Valor de la temperatura corporal de una medición en grados centígrados.

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Body temperature data was sent to manufacturer's microservice."
      }

.. note::

   La temperatura está medida en grados centígrados.
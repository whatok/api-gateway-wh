Peso
----------------------

.. http:post:: /api/v1/manufacturers/weight/

   Estructura la información del peso del usuario actual con el modelo de peso e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/weight/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-04-13T07:12:00.000Z",
            "weight": 40
         },
         {
            "date": "2022-05-06T14:11:05.023Z",
            "weight": 72.98294830322266
         },
         {
            "date": "2022-05-31T09:44:49.879Z",
            "weight": 70
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **weight** (*number*) -- Valor del peso en kilogramos de una medición.

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Weight data was sent to manufacturer's microservice."
      }

.. note::

   El peso está medido en kilogramos.
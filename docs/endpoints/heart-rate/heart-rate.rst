Frecuencia Cardíaca
----------------------

.. http:post:: /api/v1/manufacturers/heart-rate/

   Estructura la información de la frecuencia cardíaca del usuario actual con el modelo de frecuencia cardíaca e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/hear-rate/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-06-13T08:11:00.000Z",
            "heart_rate": 64
         },
         {
            "date": "2022-06-14T08:11:00.000Z",
            "heart_rate": 70
         },
         {
            "date": "2022-06-14T09:55:00.000Z",
            "heart_rate": 69
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **hear_rate** (*number*) -- Valor de la frecuencia cardíaca de una medición.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Heart rate data was sent to manufacturer's microservice."
      }
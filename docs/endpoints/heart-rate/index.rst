Frecuencia Cardíaca
============================

En este apartado se ven las diferentes rutas relacionadas con la frecuencia cardíaca del usuario.

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

   heart-rate
   resting-heart-rate
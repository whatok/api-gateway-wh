Frecuencia Cardíaca en Reposo
--------------------------------------

.. http:post:: /api/v1/manufacturers/resting-heart-rate/

   Estructura la información de la frecuencia cardíaca en reposo del usuario actual con el modelo de frecuencia cardíaca en reposo e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/resting-heart-rate/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-06-15T19:00:00.000Z",
            "resting_heart_rate": 70
         },
         {
            "date": "2022-06-16T01:00:00.000Z",
            "resting_heart_rate": 58
         },
         {
            "date": "2022-06-16T02:00:00.000Z",
            "resting_heart_rate": 54
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **resting_heart_rate** (*number*) -- Valor de la frecuencia cardíaca en reposo de una medición.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Resting heart rate data was sent to manufacturer's microservice."
      }
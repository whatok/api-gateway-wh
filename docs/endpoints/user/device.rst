Dispositivos
----------------------

.. http:post:: /api/v1/manufacturers/device/

   Estructura la información de los dispositivos del usuario actual con el modelo de dispositivos e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/device/ \
              -d @body.json

   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

         [
            {
               "users_id": 1,
               "manufacturers_id": 5
            },
            {
               "device_id": "4123467b73s22",
               "name": "Omron 3 series",
               "model_number": "BP77100",
               "first_connection": "2022-07-26T19:09:51.719Z",
               "is_connected": true,
               "connection_status_description": "Dispositivo conectado."
            }
         ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **manufacturers_id** (*number*) -- Id del fabricante del dispositivo.
         * **device_id** (*string*) -- Uid del dispositivo.
         * **name** (*string*) -- Nombre del dispositivo vinculado al usuario.
         * **model_number** (*string*) -- Modelo del dispositivo vinculado al usuario.
         * **first_connection** (*Date*) -- Fecha en que se vinculó por primera vez el dispositivo.
         * **is_connected** (*boolean*) -- Variable que indica si el dispositivo se encuentra conectado.
         * ``Opcional`` **connection_status_description** (*string*) -- Descripción de la conexión con el dispositivo.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Device data was sent to manufacturer's microservice."
      }
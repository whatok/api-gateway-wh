Usuario y dispositivos
==============================================

En este apartado se ven las diferentes rutas relacionadas con la información del usuario y sus dispositivos.

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

   user
   device
   manufacturer
   

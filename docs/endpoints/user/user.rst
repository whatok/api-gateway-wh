Perfil
-----------

.. http:post:: /api/v1/???/user/

   Estructura la información del perfil del usuario actual con el modelo de la información del usuario e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/???/user/ \
              -d @body.json

   

   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

         [
            {
               "users_id": 1,
            },
            {
               "gender": "Masculino",
               "blood_type": "O+",
               "birth_date": "2000-06-15T22:56:00.000Z",
               "email": "juan.camilo32@hotmail.com"
            } 
         ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **manufacturers_id** (*number*) -- Id del fabricante que se utilizó para obtener los datos.
         * **gender** (*string*) -- Género del usuario.
         * **blood_type** (*string*) -- Tipo de sangre del usuario.
         * **birth_date** (*Date*) -- Fecha de nacimiento del usuario.
         * **email** (*string*) -- Correo electrónico del usuario.

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "User data was sent to manufacturer's microservice."
      }
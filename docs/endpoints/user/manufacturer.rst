Fabricantes
----------------------

.. http:get:: /api/v1/manufacturers/manufacturer/

   Retorna la información (id y nombre) de los fabricantes.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/manufacturer/

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

        {
            "ok": true,
            "message": "Datos obtenidos satisfactoriamente.",
            "data": {
                "totalItems": 2,
                "manufacturers": [
                    {
                        "id": 1,
                        "name": "XIAOMI"
                    },
                    {
                        "id": 2,
                        "name": "OMRON"
                    },
                ]
            }
        }
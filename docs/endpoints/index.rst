==============================================
Rutas
==============================================

En este apartado se ven las diferentes rutas a las que se puede acceder desde la API de Whatoko Health.


.. toctree::
   :maxdepth: 1
   :caption: Contenido:

   user/index
   activity/index
   blood/index
   heart-rate/index
   body-comp/index
   sleep/index

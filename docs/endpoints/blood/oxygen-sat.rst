Saturación de Oxígeno
---------------------------

.. http:post:: /api/v1/manufacturers/oxygen-saturation/

   Estructura la información de la saturación de oxígeno del usuario actual con el modelo de saturación de oxígeno e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/oxygen-saturation/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-06-11T09:13:00.000Z",
            "oxygen_saturation": 95
         },
         {
            "date": "2022-06-12T09:13:00.000Z",
            "oxygen_saturation": 96
         },
         {
            "date": "2022-06-13T09:13:00.000Z",
            "oxygen_saturation": 97
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **oxygen_saturation** (*number*) -- Porcentaje de saturación de oxígeno de una medición.

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Oxygen saturation data was sent to manufacturer's microservice."
      }
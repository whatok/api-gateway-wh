Colesterol
----------------------

.. http:post:: /api/v1/???/cholesterol/

   Estructura la información del colesterol del usuario actual con el modelo de colesterol e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/???/cholesterol/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
         },
         {
            "date": "2022-06-14T09:11:00.000Z",
            "cholesterol": 150,
            "LDL": 100,
            "HDL": 100,
            "triglycerides": 130
         },
         {
            "date": "2022-06-14T09:11:00.000Z",
            "cholesterol": 130,
            "LDL": 120,
            "HDL": 110,
            "triglycerides": 120
         },
         {
            "date": "2022-06-14T09:11:00.000Z",
            "cholesterol": 175,
            "LDL": 120,
            "HDL": 120,
            "triglycerides": 180
         },
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **cholesterol** (*number*) -- Nivel de colesterol total en mg/dL.
         * **LDL** (*number*) -- Nivel de colesterol "malo" en mg/dL.
         * **HDL** (*number*) -- Nivel de colesterol "bueno" en mg/dL.
         * **triglycerides** (*number*) -- Nivel de trigliceridos en mg/dL.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Cholesterol data was sent to manufacturer's microservice."
      }

.. note::

   El colesterol, LDL, HDL y triglicéridos están medidos en mg/dL.
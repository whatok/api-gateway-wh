Presión Arterial
----------------------

.. http:post:: /api/v1/manufacturers/pressure/

   Estructura la información de la presión arterial del usuario actual con el modelo de presión arterial e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/pressure/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-06-11T09:05:00.000Z",
            "systolic": 117,
            "diastolic": 77
         },
         {
            "date": "2022-06-12T09:05:00.000Z",
            "systolic": 118,
            "diastolic": 78
         },
         {
            "date": "2022-06-13T09:05:00.000Z",
            "systolic": 119,
            "diastolic": 79
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **systolic** (*number*) -- Valor de la presión sistólica de una medición.
         * **diastolic** (*number*) -- Valor de la presión diastólica de una medición.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Pressure data was sent to manufacturer's microservice."
      }

.. note::

   Los niveles de presión están medidos en mmHg.
Sangre
============================

En este apartado se ven las diferentes rutas relacionadas con la sangre del usuario.

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

   glucose
   pressure
   oxygen-sat
   cholesterol
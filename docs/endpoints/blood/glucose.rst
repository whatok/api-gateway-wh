Glucosa
----------------------

.. http:post:: /api/v1/manufacturers/glucose/

   Estructura la información de la glucosa del usuario actual con el modelo de glucosa e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/glucose/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-06-14T09:11:00.000Z",
            "glucose_level": 7
         },
         {
            "date": "2022-06-14T10:24:00.000Z",
            "glucose_level": 6.900000095367432,
            "temporal_relation_to_meal": "General",
            "meal_type": "Desayuno",
            "temporal_relation_to_sleep": "Justo antes de dormir",
            "specimen_source": "Sangre"
         },
         {
            "date": "2022-06-16T14:26:00.000Z",
            "glucose_level": 6.900000095367432,
            "meal_type": "Desconocida",
            "specimen_source": "Líquido intersticial"
         },
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **glucose_level** (*number*) -- Nivel de glucosa en mg/dL de una medición.
         * ``Opcional`` **temporal_relation_to_meal** (*string*) -- Tiempo con respecto a la última comida.

            :Opciones posibles:
              - General
              - En ayuno
              - Antes de comer
              - Despues de comer
         * ``Opcional`` **meal_type** (*string*) -- Tipo de comida ingerida al rededor del momento de la medición.

            :Opciones posibles:
              - Desconocida
              - Desayuno
              - Almuerzo
              - Cena
              - Bocadillo
         * ``Opcional`` **temporal_relation_to_sleep** (*string*) -- Tiempo con respecto a la última vez que durmió la persona.

            :Opciones posibles:
              - Despierto
              - Justo antes de dormir
              - Durante el sueño
              - Justo despues de despertar
         * ``Opcional`` **specimen_source** (*string*) -- Tipo de fluido corporal que se utilizó para medir la glucosa

            :Opciones posibles:
              - Liquido intersticial
              - Sangre capilar
              - Plasma
              - Suero
              - Lagrimas
              - Sangre

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Glucose data was sent to manufacturer's microservice."
      }

.. note::

   El nivel de glucosa está medido en mg/dL.
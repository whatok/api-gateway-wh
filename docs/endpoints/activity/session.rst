Sesiones de actividad
---------------------------

.. http:post:: /api/v1/manufacturers/session/

   Estructura la información de las sesiones de actividad del usuario actual con el modelo de sesiones e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/session/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "start_date": "2022-06-16T07:43:45.807Z",
            "end_date": "2022-06-16T08:13:45.807Z",
            "name": "prueba",
            "description": "nota de prueba",
            "activity_type": "Horseback riding",
            "distance": 50000,
            "calories": 300
         },
         {
            "start_date": "2022-06-12T02:54:47.678Z",
            "end_date": "2022-06-12T03:03:41.514Z",
            "name": "test",
            "description": "",
            "activity_type": "Walking",
            "distance": 225.3082275390625,
            "calories": 27
         },
         {
            "start_date": "2022-06-16T19:19:00.000Z",
            "end_date": "2022-06-16T19:59:00.000Z",
            "name": "Others",
            "description": "",
            "activity_type": "Other (unclassified fitness activity)",
            "distance": 68,
            "calories": 0.411264032125473
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **start_date** (*Date*) -- Fecha de inicio de una medición.
         * **end_date** (*Date*) -- Fecha final de una medición.
         * **activity_type** (*string*) -- Tipo de actividad de la sesión. Ver :doc:`actividades`.
         * **name** (*string*) -- Nombre de la sesión de actividad.
         * ``Opcional`` **description** (*string*) -- Descripción de la sesión de actividad.
         * ``Opcional`` **distance** (*number*) -- Valor de distancia recorrida en metros durante la sesión de actividad.
         * ``Opcional`` **calories** (*number*) -- Valor de calorías quemadas durante la sesión de actividad.

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Session data was sent to manufacturer's microservice."
      }

.. note::

   La distancia recorrida está medida en metros.
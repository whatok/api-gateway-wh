Tipos de actividad
---------------------------

A continuación se definen los tipos de actividad posibles para una sesión de actividad.

+-------------+
| Actividades | 
+=============+
| Atletismo   |
+-------------+
| Baloncesto  |
+-------------+
| Beisbol     |
+-------------+
| Bolo        |
+-------------+ 
| Caminar     |
+-------------+
| Ciclismo    |
+-------------+
| Futbol      |
+-------------+
| Futbol Sala |
+-------------+ 
| Golf        |
+-------------+
| Gimnasia    |
+-------------+
| Micro Futbol|
+-------------+
| Tejo        |
+-------------+ 
| Tenis       |
+-------------+
| Trotar      |
+-------------+
| Natacion    |
+-------------+
| Voleibol    |
+-------------+ 

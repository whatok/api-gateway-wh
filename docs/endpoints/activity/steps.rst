Pasos
----------------------

.. http:post:: /api/v1/manufacturers/steps/

   Estructura la información de los pasos del usuario actual con el modelo de pasos e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/steps/ \
              -d @body.json

   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "start_date": "2022-05-31T10:40:06.773Z",
            "end_date": "2022-05-31T10:43:04.773Z",
            "steps": 89
         },
         {
            "start_date": "2022-05-31T10:43:04.773Z",
            "end_date": "2022-05-31T10:44:22.028Z",
            "steps": 125
         },
         {
            "start_date": "2022-05-31T10:54:15.664Z",
            "end_date": "2022-05-31T10:55:15.681Z",
            "steps": 30
         }
      ]


   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **start_date** (*Date*) -- Fecha de inicio de una medición.
         * **end_date** (*Date*) -- Fecha final de una medición.
         * **steps** (*number*) -- Cantidad de pasos de una medición.

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Steps data was sent to manufacturer's microservice."
      }

Actividad Física
============================

En este apartado se ven las diferentes rutas relacionadas con la actividad física del usuario.

.. toctree::
   :maxdepth: 2
   :caption: Contenido:

   steps
   calories
   distance
   session
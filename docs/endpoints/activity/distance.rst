Distancia
----------------------

.. http:post:: /api/v1/manufacturers/distance/

   Estructura la información de la distancia recorrida del usuario actual con el modelo de distancia e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/distance/ \
              -d @body.json

   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "start_date": "2022-05-31T10:40:06.773Z",
            "end_date": "2022-05-31T10:43:04.773Z",
            "distance": 58.06938552856445
         },
         {
            "start_date": "2022-05-31T10:43:04.773Z",
            "end_date": "2022-05-31T10:43:52.105Z",
            "distance": 53.470211029052734
         },
         {
            "start_date": "2022-05-31T10:43:52.105Z",
            "end_date": "2022-05-31T10:44:22.028Z",
            "distance": 37.52400207519531
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **start_date** (*Date*) -- Fecha de inicio de una medición.
         * **end_date** (*Date*) -- Fecha final de una medición.
         * **distance** (*number*) -- Valor de distancia recorrida en metros de una medición.

   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Distance data was sent to manufacturer's microservice."
      }

.. note::

   La distancia recorrida está medida en metros.
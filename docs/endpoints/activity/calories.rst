Calorías
----------------------

.. http:post:: /api/v1/manufacturers/calories/

   Estructura la información de las calorías quemadas del usuario actual con el modelo de calorías e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/calories/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "start_date": "2022-04-12T07:13:00.000Z",
            "end_date": "2022-04-13T07:12:00.000Z",
            "calories": 1058.76416015625
         },
         {
            "start_date": "2022-05-06T14:11:05.023Z",
            "end_date": "2022-05-06T14:11:06.635Z",
            "calories": 0.029207322746515274
         },
         {
            "start_date": "2022-05-29T09:44:49.879Z",
            "end_date": "2022-05-31T09:44:55.798Z",
            "calories": 3131.015869140625
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **start_date** (*Date*) -- Fecha de inicio de una medición.
         * **end_date** (*Date*) -- Fecha final de una medición.
         * **calories** (*number*) -- Valor de calorías quemadas de una medición.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Calories data was sent to manufacturer's microservice."
      }
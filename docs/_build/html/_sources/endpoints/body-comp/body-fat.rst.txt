Porcentaje de Grasa
----------------------

.. http:post:: /api/v1/manufacturers/body-fat/

   Estructura la información del porcentaje de grasa del usuario actual con el modelo de porcentaje de grasa e ingresa la información a la base de datos.

   **Ejemplo de solicitud**

   .. tabs::

        .. code-tab:: bash

            $ curl -H "x-token: <token>" https://localhost:8000/api/v1/manufacturers/body-fat/ \
              -d @body.json


   **Ejemplo del contenido de** ``body.json``

   .. sourcecode:: json

      [
         {
            "users_id": 1,
            "device_id": 5
         },
         {
            "date": "2022-01-11T09:54:00.000Z",
            "body_fat": 11
         },
         {
            "date": "2022-01-12T09:55:00.000Z",
            "body_fat": 12
         },
         {
            "date": "2022-01-13T09:55:00.000Z",
            "body_fat": 13
         }
      ]

   .. note::

      :Contenido del body:

         * **users_id** (*number*) -- Id del usuario al que pertenecen los datos.
         * **device_id** (*number*) -- Id del dispositivo que se utilizó para obtener los datos.
         * **date** (*Date*) -- Fecha de una medición.
         * **body_fat** (*number*) -- Valor de porcentaje de grasa de una medición.


   **Ejemplo de respuesta exitosa**

   .. sourcecode:: json

      {
         "ok": true,
         "message": "Body fat data was sent to manufacturer's microservice."
      }
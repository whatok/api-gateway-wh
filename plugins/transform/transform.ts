'use strict';
import kong from "kong-pdk/kong";


/**
 * Clase del plugin de transformación de petición
 */
class Transform {

    /**
     * Función que transforma una petición
     */
    async access(kong: kong) {
        kong.service.request.addHeader("connection", "keep-alive")
        kong.service.request.addHeader("cache-control", "no-cache")
        kong.service.request.addHeader("content-type", "application/json")
        kong.service.request.addHeader("accept", "*/*")
        kong.service.request.addHeader("Scrict-Transport-Security", "max-age=31536000")
        kong.service.request.addHeader("Strict-Transport-Security", "includeSubDomains")
        kong.service.request.addHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
        kong.service.request.addHeader('Access-Control-Allow-Origin', 'http://localhost:4200')
        kong.service.request.addHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
        kong.service.request.addHeader('Access-Control-Allow-Credentials', true)
    };

}

module.exports = {
    Plugin: Transform,
    Schema: [],
    Version: "0.1.0",
    Priority: 10,
};
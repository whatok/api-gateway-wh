'use strict'
import { error } from "console";
import iconv from 'iconv-lite';
import kong from "kong-pdk/kong";
import { Client, Pool } from 'pg';


/**
//  * Clase del plugin Logging
 */
class Logging {

  //**Variables de pluging */
  config: any;

  /**Elemento para almacenar la petición */
  req: any;

  dbConfig: any;

  pool: any;


  /**
   * Crea una instancia de la clase Logging
   */
  constructor(config: any) {
    this.config = config;

    this.dbConfig = {
      user: 'kong',
      host: 'kong-database',
      database: 'kong',
      password: 'kong',
      port: 5432
    };

    this.pool = new Pool(this.dbConfig)

  }

  private async createDB() {
    this.pool.query(`
    CREATE TABLE IF NOT EXISTS logs (
        id SERIAL PRIMARY KEY,
        date timestamp with time zone,
        method TEXT,
        service TEXT,
        req_body TEXT,
        res_status NUMERIC
    )
    `);
  }



  /**
   * Función que toma almacena los logs de las peticiones
   */
  async access(kong: kong) {

    await this.createDB()
    let body: any;
    body = await kong.request.getRawBody();

    if (body.length === 0) {
      // No hay cuerpo en la solicitud
      body = "{}";
    }

    const request = {
      method: await kong.request.getMethod(),
      url: await kong.request.getPath(),
      headers: await kong.request.getHeaders(),
      body
    };
    this.req = request;
  }

  /**
   * Función que toma los logs de las respuestas
   * @param kong 
   */
  async response(kong: kong) {

    const res = {
      status: await kong.response.getStatus(),
      headers: await kong.response.getHeaders()
    };

    const date = new Date().toLocaleString();
    this.pool.query(`INSERT INTO logs(date, method, service, req_body, "res_status") VALUES('${date}', '${this.req.method}', '${this.config.servicio}', '${JSON.stringify(JSON.parse(this.req.body))}', ${res.status})`)
  }

}

module.exports = {
  Plugin: Logging,
  Schema: [
    { servicio: { type: "string", required: true } }
  ],
  Version: "0.1.0",
  Priority: 11,
};
'use strict'
import kong from "kong-pdk/kong";
import jwt, { JwtPayload } from "jsonwebtoken"

/**
 * Clase del plugin de autenticación
 */
class Auth {

    /**
     * Función que verifica la validez de un token
     * @param kong 
     * @returns id de usuario
     */
    private async validateJwt(kong: kong) {
        let token = await this.getJwt(kong);

        if (token == "") {
            return kong.response.error(401, "Unauthorized by Kong", { ["Content-Type"]: "application/json" })
        }

        try {
            const { id } = jwt.verify(token, "Est03sMypub1ck3y12@07071999"!) as JwtPayload
            kong.service.request.addHeader("id", id);

        } catch (error) {
            return kong.response.error(401, "Unauthorized by Kong", { ["Content-Type"]: "application/json" })
        }
    }

    /**
     * Método para obtener el token de los headers
     * @param kong 
     * @returns Token de la petición en String
     */
    private async getJwt(kong: kong) {
        let token = kong.request.getHeader("x-token")
        return token.then(value => { return value; })
    }

    /**
     * Función que toma y valida el token antes de enviar la petición a los servicios
     * @param kong 
     */
    async access(kong: kong) {
        // kong.service.request.addHeader
        await this.validateJwt(kong);
    }
}

module.exports = {
    Plugin: Auth,
    Schema: [],
    Version: "0.1.0",
    Priority: 0
}
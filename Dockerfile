FROM kong/kong-gateway:2.8.1.0-alpine

#Instalación de dependencias
USER root

RUN apk update \
    && apk add curl gnupg  \
    && curl -sL https://deb.nodesource.com/setup_16.x | bash \
    && apk add nodejs npm
RUN apk add python3 
RUN apk add make 
RUN apk add g++
RUN rm -rf /var/cache/apt/*

# Copia de plugins e instalación de Kong-pdk
COPY plugins /usr/local/kong/plugins
WORKDIR /usr/local/kong/plugins
RUN npm install --unsafe -g kong-pdk@0.5.3 \
    && npm install 
ENV term xterm
RUN apk update
RUN apk add vim nano


# #Copia y build de docs
# COPY /docs /usr/local/docs
# WORKDIR /usr/local/docs
# RUN apk add py3-pip
# RUN pip3 install -U sphinx --ignore-installed
# RUN pip3 install -r requirements.txt
# RUN make clean html



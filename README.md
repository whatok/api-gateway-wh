[![N|Solid](https://cdn.connectamericas.com/sites/default/files/styles/company_public_profile_picture/public/company-26786-1618066328.png?itok=W_hmtPq3)](http://aiatic.com/)

# API Gateway WH

_Microservicios y core backend del proyecto Whatoko Health_

[![Version](https://badgen.net/badge/Project%20Version/1.0.0/green)](http://aiatic.com/) [![License](https://badgen.net/badge/License/Apache%202.0/blue)](http://aiatic.com/) [![Node Version](https://badgen.net/badge/Node.js/18.x.x/green)](https://nodejs.org/es/)

### Project structure

```sh
api-gateway-wh
├── .k8s
    ├── kong
    ├── manifests
├── config
├── docs
├── plugins
    ├─── auth     
        ├─── package.json
        ├─── tsconfig.json
```

## Starting 🚀

Antes de clonar un proyecto, se deberá asegurar la conexión con el mismo y las medidas de seguridad requeridas.

```bash
ssh-keygen -t rsa -b 4096 -C "correoDelDesarrollado@aiatic.com"
Colocar clave
eval $(ssh-agent -s)
ssh-add ~/.ssh/id_rsa
ssh -T git@gitlab.com (testear conexion ssh) Deberá decir yes
```

_El repositorio debe ser clonado con el comando_

`$ git clone https://gitlab.com/whatok/api-gateway-wh.git`

_Se debe configurar el usuario adecuado para el proyecto (Ejecutar las siguientes lineas dentro del proyecto)_

```bash
git config user.name "Nombre del desarrollador"
git config user.email "correoDelDesarrollado@aiatic.com"
```

### Pre requerimientos 📋

_docker version 20.X.X_

```bash
https://docs.docker.com/get-docker/
```

_docker-compose version 1.x.x_

```bash
https://docs.docker.com/compose/install/
```

### Installation 🔧

_Siga los siguientes pasos para levantar el ambiente de desarrollo requerido para api-gateway-wh_

```bash
git clone https://gitlab.com/whatok/api-gateway-wh.git
```

Este repositorio debe quedar en su respectivo directorio, una vez descargado procederemos a ejecutar:


_Ingresamos al proyecto de api-gateway-wh_

```bash
docker-compose up -d --build
```

De este modo ejecutando el build del repositorio se tendrá el API Gateway de Whatoko Health 

## Built with 🛠️
Tools implemented in development.

- [Kong](https://docs.konghq.com/gateway/latest/) - Kong Gateway is a lightweight, fast, and flexible cloud-native API gateway. An API gateway is a reverse proxy that lets you manage, configure, and route requests to your APIs.
- [Node.js](https://nodejs.org/) - Cross-platform runtime environment.
- [Docker](https://www.docker.com/) - Automate application deployment within software containers.
- [TypeScript](https://typescriptlang.org/) - is a strongly typed programming language that builds on JavaScript.
- [JWT](https://jwt.io/) - JSON Web Token (JWT) is a compact URL-safe means of representing claims to be transferred between two parties.
- [Kong-PDK](https://docs.konghq.com/gateway/latest/plugin-development/pdk/) - The Plugin Development Kit (PDK) is set of Lua functions and variables that can be used by plugins to implement their own logic.

## Integrations
Integrated development tools.
- [Node.js](https://nodejs.org/) - Cross-platform runtime environment.

## Documentación API Gateway

[Documentación API Gateway](https://api-gateway-wh.readthedocs.io/en/latest/)


## Documentación general
:bookmark_tabs: <a href="https://aiatic.atlassian.net/wiki/spaces/WH/overview?homepageId=1212619" target="_blank">Documentación en Confluence</a>

---

⌨️ por [A&A](http://aiatic.com/) 😊

**All rights reserved**

[//]: # "These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax"


## Documentación

[Documentación API Gateway](https://api-gateway-wh.readthedocs.io/en/latest/)
